import urllib2
import sys
import os

files=open('data/laws/files.txt')
prefix='http://www.gpo.gov/fdsys/pkg/PLAW-'
text_middle='/html/PLAW-'
meta_postfix='/mods.xml'
text_postfix='.htm'

for line in files:
    line=line.strip()
    print line
    line=line.split('/')
    congress=line[0].split('-')[1]
    doc=line[1][:-4]
    text_url=prefix+congress+doc+text_middle+congress+doc+text_postfix
    meta_url=prefix+congress+doc+meta_postfix
    path='data/laws/'+congress+'/'
    if not os.path.isdir(path):
        os.mkdir(path)
        os.mkdir(path+'text')
        os.mkdir(path+'meta')
    fout=open(path+'text/'+line[1],'w')
    text_url=urllib2.urlopen(text_url)
    fout.write(text_url.read())
    fout.close()
    fout=open(path+'meta/'+line[1][:-4]+'.xml','w')
    meta_url=urllib2.urlopen(meta_url)
    fout.write(meta_url.read())
    fout.close()
