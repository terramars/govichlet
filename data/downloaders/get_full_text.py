import os
from xml.etree import cElementTree as etree
import re
import sys

strip_unicode=re.compile("([^-_a-zA-Z0-9!@#%&=,/'\";:~`\$\^\*\(\)\+\[\]\.\{\}\|\?\<\>\\]+|[^\s]+)")

infolder='/mnt/bacon/data/bills/'
textfolder='/mnt/bacon/data/laws/'
outfolder='/mnt/bacon/data/fulltext/'

congresses=os.listdir(infolder)
for congress in congresses:
    inpath=infolder+congress+'/ENACTED/'
    textpath=textfolder+congress+'/text/'
    outpath=outfolder+congress+'/'
    if not os.path.isdir(outpath):
        os.mkdir(outpath)
    if 1:
        files=os.listdir(inpath)
        for f in files:
            data=etree.parse(inpath+f)
            root=data.getroot()
            number=root.find('actions').find('enacted').get('number')
            number=str(int(number.split('-')[1]))
            text=strip_unicode.sub('',open(textpath+'publ'+number+'.htm').read().strip())
            fout=open(outpath+f[:-4]+'_'+number+'.htm','w')
            print fout.name
            fout.write(text)
            fout.close()
