import urllib2
import sys
import os
from xml.etree import cElementTree as etree

prefix='http://www.govtrack.us/data/us/'
middle='/bills/'
postfix='.xml'
base_path='/mnt/bacon/data/bills/'

to_download=sys.argv[1:]

for congress in to_download:
    path=base_path+congress+'/'
    if not os.path.isdir(path):
        os.mkdir(path)
    print 'downloading congress : '+congress
    for side in ('h','hc','hj','hr','s','sc','sj','sr'):
        print 'using bills from : ',side
        i=1
        j=0
        statuses={}
        while j<5:
            bill=side+str(i)
            url=prefix+congress+middle+bill+postfix
            data=''
            try:
                data=urllib2.urlopen(url)
                data=data.read()
            except Exception:
                j+=1
                i+=1
                print j,'error : ',url
                continue
            if j:
                j=0
            root=etree.fromstring(data)
            state=root.find('state').text
            if state not in statuses:
                statuses[state]=1
            else:
                statuses[state]+=1
            print state+' '+bill+' total '+str(statuses[state])
            if state.find(':')>-1:
                state=state.split(':',1)[0]
            if not os.path.isdir(path+state):
                os.mkdir(path+state)
            fout=open(path+state+'/'+bill+'.xml','w') 
            fout.write(data)
            fout.close()
            i+=1
            continue
        print 'congress '+congress+' side '+side+' done downloading'
        print 'final tally : '
        print statuses.items()

