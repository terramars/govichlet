import sys
import cPickle as cp

topic_file=open(sys.argv[1])
ids=sys.argv[2]
fout=sys.argv[3]

ids=cp.load(open(ids))
ids={j:i for i,j in ids.items()}

topics=[]
norm=0.0

for line in topic_file:
    line=[float(i) for i in line.strip().split()]
    total=sum(line)
    norm+=total
    weights=[(i,line[i]/total) for i in xrange(len(line))]
    weights.sort(key=lambda x:x[1],reverse=True)
    topics.append((total,weights[:100]))

print norm
topics=[(i[0]/norm,[(ids[j[0]],j[1]) for j in i[1]]) for i in topics]
cp.dump(topics,open(fout,'w'),cp.HIGHEST_PROTOCOL)

