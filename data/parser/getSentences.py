#!/usr/bin/python

import sys
import time
import nltk.corpus as corpus
import string
from collections import defaultdict
import cPickle as cp
import re
import os

''' Generate string for OHDP w/ word ids, word counts '''
stopwords = corpus.stopwords.words('english')+['act','sec','title','amended','introduced','passed','version','summary','subtitle','ii','iii','iv','vi','vii','viii','ix','lt','gt','doc','docid','page','stat','note','html','pre','body','section','table','contents','short','usc','part','et','seq']
punc = [i for i in string.punctuation+string.ascii_lowercase]
stop = set(stopwords+punc)

def parse_sentence(sent):
    newsent=[]
    for word in sent:
        word=word.lower()
        if word not in stop and not word.isdigit() and word.isalnum():
            newsent.append(word)
    if len(newsent):
        return newsent
    return None

def parse_paragraph(para):
    newpara=[]
    for sent in para:
        newsent=parse_sentence(sent)
        if newsent:
            newpara.append(newsent)
    if len(newpara):
        return newpara
    return None

def sentence_to_id_list(sent,ids,next_id):
    idsent = []
    if not len(sent):
        print 'invalid sentence'
        return idsent,next_id
    for word in sent:
        if word not in ids:
            ids[word]=[next_id,0]
            next_id+=1
        ids[word][1]+=1
        idsent.append(ids[word][0])
    return (idsent,next_id)

def paragraph_to_id_list(para,ids,next_id):
    idpara=[]
    if not len(para):
        print 'invalid paragraph'
        return idpara,next_id
    for sent in para:
        idsent,next_id=sentence_to_id_list(sent,ids,next_id)
        idpara.append(idsent)
    return idpara,next_id

def document_to_id_list(body,ids,next_id):
    idbody=[]
    i=-1
    for para in body:
        i+=1
        para=parse_paragraph(para)
        if not para:
            continue
        idpara,next_id=paragraph_to_id_list(para,ids,next_id)
        idbody.append(idpara)
    return idbody,next_id

def convert_corpus_to_ids(wordlists,output_folder,ids=None):
    if not ids:
        ids={}
    fileids=wordlists.fileids()
    for f in fileids:
        folder=f.rsplit('/',1)[0]
        if folder==f:
            continue
        if os.path.isdir(output_folder+folder):
            continue
        folder=folder.split('/')[-2:]
        current=''
        for item in folder:
            current+=item
            if not os.path.isdir(output_folder+current):
                print 'creating folder',output_folder+current
                os.mkdir(output_folder+current)
            current+='/'
    next_id=0
    if len(ids):
        next_id=len(ids)
    j=0
    bad=0
    for f in fileids:
        print j,'converting ',f,'\tto ids, current word count : ',next_id
        j+=1
        body=wordlists.paras(f)
        try:
            idbody,next_id=document_to_id_list(body,ids,next_id)
            if not len(idbody):
                bad+=1
                continue
            cp.dump(idbody,open(output_folder+f+'.id','w'))
        except Exception:
            print 'error on ',f
    print 'total',j,'bad',bad,'words',next_id
    return ids

def main():
    corpus_root=sys.argv[1]
    corpus_glob=sys.argv[2]
    output_folder=sys.argv[3]
    id_output=sys.argv[4]
    ids={}
    if len(sys.argv)>5:
        ids=cp.load(open(sys.argv[5]))
    print 'converting text to ids'
    wordlists=corpus.PlaintextCorpusReader(corpus_root,corpus_glob)
    ids=convert_corpus_to_ids(wordlists,output_folder,ids)
    print 'saving Word:ID correspondence'
    cp.dump(ids,open(id_output,'w'))
    return

if __name__=='__main__':
    main()
