import cPickle as cp
import sys
import numpy
import re
from collections import defaultdict


def load_words(fwords):
    #words=cp.load(open(fwords))
    fwords=open(fwords)
    ids={}
    words={}
    i=0
    for word in fwords:
        word=word.strip()
        ids[i]=word
        words[word]=i
        i+=1
    return words,ids

def make_counts(fin):
    fin=open(fin)
    splitexp=re.compile(r'[ :]')
    #ids={j:i for i,j in words.items()}
    j=0
    counts=defaultdict(int)
    for line in fin:
        line=line.strip()
        if j%10000==0:
            print 'counting',j
        j+=1
        if not line:
            continue
        splitline=[int(i) for i in splitexp.split(line[2:])]
        wordids = splitline[2::2]
        wordcts = splitline[1::2]
        for i in xrange(len(wordids)):
            counts[wordids[i]]+=wordcts[i]
    fin.close()
    return counts

def make_df(fin):
    fin=open(fin)
    splitexp=re.compile(r'[ :]')
    j=0
    counts=defaultdict(int)
    for line in fin:
        line=line.strip()
        if j%10000==0:
            print 'counting',j
        j+=1
        if not line:
            continue
        splitline=[int(i) for i in splitexp.split(line[2:])]
        wordids = splitline[2::2]
        wordids=set(wordids)
        for i in wordids:
            counts[i]+=1
    fin.close()
    return counts

def find_rare_words(ids,counts,thresh=5):    
    singleids = {i:ids[i] for i,j in counts.items() if j<thresh}
    nsingles=len(singleids.keys())
    print 'words that appear rarely :',nsingles
#    print ', '.join(singleids.values())
    return singleids

def find_common_words(ids,counts,ndocs,thresh=0.2):
    commonwords= {i:ids[i] for i,j in counts.items() if float(j)/ndocs>thresh}
    toprint={j:counts[i]/float(ndocs) for i,j in commonwords.items()}
    toprint=toprint.items()
    toprint.sort(key=lambda x:x[1],reverse=1)
    ncommon=len(commonwords.keys())
    print 'frequently occurring words : ',ncommon
    print ', '.join([str(i) for i in toprint])
    return commonwords

def merge_rare_and_common(singleids,commonwords):
    for key,val in commonwords.items():
        singleids[key]=val

def get_new_mapping(singleids,ids):
    mapping={}
    ntotal=len(ids.keys())
    newct=0
    for i in xrange(ntotal):
        if i in singleids:
            continue
        mapping[i]=newct
        newct+=1
    return mapping

#ntotal=len(words.keys())
#ngood=ntotal-nsingles

def get_new_word_dicts(words,mapping,singleids):
    words = {i:mapping[j] for i,j in words.items() if j not in singleids}
    ids = {j:i for i,j in words.items()}
    return words,ids

def write_new_words(fwords,ids):
    fwords=open(fwords,'w')
    for i in xrange(len(ids)):
        fwords.write(ids[i]+'\n')
    fwords.close()

#cp.dump(words,open(fwords,'w'))

def write_new_ids(fin,fout,singleids,mapping,files,counts=None,thresh=6):
    print 'writing new ids'
    fin=open(fin)
    fout=open(fout,'w')
    splitexp=re.compile(r'[ :]')
    j=-1
    for line in fin:
        line=line.strip()
        j+=1
        if not line:
            del files[j]
            continue
        if j%10000==0:
            print 'writing',j
        splitline=[int(i) for i in splitexp.split(line[2:])]
        wordids = splitline[2::2]
        wordcts = splitline[1::2]
        newline='| '
        ngood=0
        for i in xrange(len(wordids)):
            if wordids[i] in singleids:
                continue
            ngood+=1
            if counts:
                newline+='%d:%g '%(mapping[wordids[i]],wordcts[i]*counts[wordids[i]])
            else:
                newline+='%d:%d '%(mapping[wordids[i]],wordcts[i])
        if ngood<thresh:
            del files[j]
            continue
        newline=newline.strip()
        #if not newline:
        #    continue
        fout.write(newline+'\n')
    fout.close()
    fin.close()

def load_files(ffiles):
    files={}
    i=0
    for line in open(ffiles):
        line=line.strip()
        files[i]=line
        i+=1
    return files

def write_new_files(files,fileout):
    fileout=open(fileout,'w')
    for _,f in files.items():
        fileout.write(f+'\n')
    fileout.close()

def change_counts_to_idf(counts,ndocs):
    for i,j in counts.items():
        counts[i]=numpy.log(ndocs/j)
    return counts


if __name__=='__main__': 
    fwords=sys.argv[1]
    wordout=sys.argv[2]
    fin=sys.argv[3]
    fout=sys.argv[4]
    ffiles=sys.argv[5]
    fileout=sys.argv[6]
    thresh=.3
    if len(sys.argv)>7:
        thresh=float(sys.argv[7])
    words,ids=load_words(fwords)
    files=load_files(ffiles)
    counts=make_df(fin)
    singleids=find_rare_words(ids,counts,3)
    commonwords=find_common_words(ids,counts,len(files),thresh=thresh)
    merge_rare_and_common(singleids,commonwords)
    mapping=get_new_mapping(singleids,ids)
    words,ids=get_new_word_dicts(words,mapping,singleids)
    counts=change_counts_to_idf(counts,len(files))
    write_new_words(wordout,ids)
    write_new_ids(fin,fout,singleids,mapping,files)
    write_new_files(files,fileout)

