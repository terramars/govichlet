#!/usr/bin/python

import sys
import nltk.corpus as corpus
import string
from collections import defaultdict
import cPickle as cp
import re
import os

''' Generate string for OHDP w/ word ids, word counts '''
stopwords = corpus.stopwords.words('english')#+['act','sec','secretary','requires','federal','authorizes','national','amends','directs','states','state','public','title','law','program','specified','services','service','amended','introduced','passed','requires','version','measure','summary','since']
punc = [i for i in string.punctuation+string.ascii_lowercase]
stop = set(stopwords+punc)
stop = set(punc)

def main():
    # Input
    id_output = sys.argv[1]
    file_output = open(sys.argv[2],'w')
    word_output = open(sys.argv[3],'w')
    folders=sys.argv[4:]
    
    id_dict={}
    file_dict={}
    nfile=0
    counter=0
    out = open(id_output,'w')
    inputs=[]
    for f in folders:
        inputs+=[(f,i) for i in os.listdir(f) if i.endswith('txt')]

    print len(inputs)
    for folder,name in inputs:
        inp=folder+name
        file_output.write(name+'\n')
        outstr,counter = parse(inp,id_dict,counter,word_output)
        nfile+=1
        print nfile,counter,inp
        out.write(outstr+'\n')
    
    out.close()
    file_output.close()
    word_output.close()
    
    #f = open(word_pickle,'w')
    #cp.dump(id_dict,f,cp.HIGHEST_PROTOCOL)
    #f.close()

    #f = open(file_pickle,'w')
    #cp.dump(file_dict,f,cp.HIGHEST_PROTOCOL)
    #f.close()


def parse(inp, id_dict, counter,word_output):
    f = open(inp)
    #raw = f.readlines()
    # Filter out punctuation
    #words = nltk.word_tokenize(raw[0])
    words=[]
    for line in f:
        words+=re.findall(r'\w+(?:\w+)?',line.lower())
    
    words = filter(lambda x : x not in stop and x.isalpha(), words)
    
    # Word <--> ID
    word_counts = dict()
    for word in words:
        if word not in id_dict.keys():
            id_dict[word] = counter
            counter += 1
            word_output.write(word+'\n')
        my_id = id_dict[word]
        if my_id not in word_counts.keys():
           word_counts[my_id] = 1 
        else:
           word_counts[my_id] += 1

    output_string = ''
    for k,v in word_counts.items():
        output_string += str(k) + ':' + str(v) + ' '

    output_string = output_string[:-1]
    return output_string,counter

if __name__ == "__main__":
    main()
