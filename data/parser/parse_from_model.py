import sys

def load_words(fwords):
    data=[i.strip() for i in open(fwords).readlines()]
    words={}
    for i in xrange(len(words)):
        words[data[i]]=i
    return words

def get_counts(fin,words):
    fin=open(fin)
    counts={}
    data=[]
    for line in fin:
        data+=re.findall(r'\w+(?:\w+)?',line.lower())
    data = filter(lambda x : x not in stop and x.isalpha(), data)
    for word in data:
        if word not in words:
            continue
        id=words[word]
        if id not in counts:
            counts[id]=1
        else:
            counts[id]+=1
    return counts

def get_doc_string(counts):
    output_string = str(len(counts))+' '
    for k,v in counts.items():
        output_string += str(k) + ':' + str(v) + ' '

    output_string = output_string[:-1]
    return output_string

if __name__=='__main__':
    fin=sys.argv[1]
    fout=sys.argv[2]
    fwords=sys.argv[2]
    words=load_words(fwords)
    counts=get_counts(fin,words)
    output=get_doc_string(counts)
    open(fout,'w').write(output)
