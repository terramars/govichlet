#!/usr/bin/python

import sys
import nltk.corpus as corpus
import string
from collections import Counter
import cPickle as cp
import re
import os

''' Generate string for OHDP w/ word ids, word counts '''
#stopwords = corpus.stopwords.words('english')#+['act','sec','secretary','requires','federal','authorizes','national','amends','directs','states','state','public','title','law','program','specified','services','service','amended','introduced','passed','requires','version','measure','summary','since']
#punc = [i for i in string.punctuation+string.ascii_lowercase]
#stop = set(stopwords+punc)
#stop = set(punc)

def load_sentences_to_counts(sents,counts=None):
    if not counts:
        counts=Counter()
    for sent in sents:
        for word in sent:
            counts[word]+=1
    return counts

def load_document_to_counts(paras):
    counts=Counter()
    for para in paras:
        counts=load_sentences_to_counts(para,counts)
    return counts

def write_output_string(counts):
    output_string = '| '
    for k,v in counts.items():
        output_string += str(k) + ':' + str(v) + ' '
    output_string = output_string[:-1]
    return output_string

def convert_word_pickle(words,wordout):
    words={j[0]:i for i,j in words.items()}
    for i in xrange(len(words)):
        wordout.write(words[i]+'\n')
    wordout.close()

def write_file_output(inputs,fileout,congress=True):
    for folder,name in inputs:
        if congress:
            name=name.split('.',1)[0]
            folder=folder.split('/')
            try:
                c=folder[-3]
                o=folder[-2]
            except Exception:
                print folder
                sys.exit()
            fileout.write('/'.join((c,o,name))+'\n')
        else:
            if not folder[-1]=='/':
                folder+='/'
            fileout.write(folder+name+'\n')
    fileout.close()

def write_id_output(inputs,idout):
    j=0
    for folder,name in inputs:
        if j%1000==0:
            print j,folder+name
        j+=1
        try:
            paras=cp.load(open(folder+name))
        except Exception:
            print folder+name
            print cp.load(open(folder+name))
            sys.exit()
        counts=load_document_to_counts(paras)
        output_str=write_output_string(counts)
        idout.write(output_str+'\n')
    idout.close()

def main(word_input,id_output,file_output,word_output,folders):
    
    convert_word_pickle(cp.load(open(word_input)),word_output)

    inputs=[]
    for f in folders:
        inputs+=[(f,i) for i in os.listdir(f) if i.endswith('id')]
    write_file_output(inputs,file_output,True)

    print len(inputs)
    write_id_output(inputs,id_output)


if __name__ == "__main__":
    word_input = sys.argv[1]
    id_output = open(sys.argv[2],'w')
    file_output = open(sys.argv[3],'w')
    word_output = open(sys.argv[4],'w')
    folders=sys.argv[5:]
    main(word_input,id_output,file_output,word_output,folders)
