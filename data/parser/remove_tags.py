import sys
import os

def remove_all_tags(text,start,end):
    lower=text.lower()
    startchars=len(start)
    endchars=len(end)
    nchars=len(text)
    newtext=''
    while 1:
        n=lower.find(start)
        if n==-1:
            newtext+=text
            break
        newtext+=text[:n]
        lower=lower[n+startchars:]
        text=text[n+startchars:]
        n=lower.find(end)
        if n==-1:
            break
        text=text[n+endchars:]
        lower=lower[n+endchars:]
    return newtext

def remove_xml_except_title(text):
    lower=text.lower()
    start='<'
    end='>'
    nstart=len(start)
    nend=len(end)
    newtext=''
    while 1:
        m=lower.find(start)
        n=lower.find(end)
        if m==-1 or n==-1:
            newtext+=text
            break
        title=lower[m:n].find('title')
        meta=lower[m:n].find('meta')
        if (title>-1 and title<3) or (meta>-1 and meta<3):
            newtext+=text[:n+nend]
            lower=lower[n+nend:]
            text=text[n+nend:]
            continue
        if lower[m:n].find('br')==0:
            newtext+='\n'
        newtext+=text[:m]
        text=text[n+nend:]
        lower=lower[n+nend:]
    return newtext


if __name__=='__main__':
    output_folder=sys.argv[2]
    input_folder=sys.argv[1]
    to_remove=sys.argv[3:]
    files=os.listdir(input_folder)
    if to_remove[0]=='xml':
        pairs=None
    else:
        pairs=[i.split(',') for i in to_remove]
        print pairs
    i=0
    for name in files:
        if i%1000==0:
            print i,name
        i+=1
        fin=open(input_folder+name)
        fout=open(output_folder+name,'w')
        text=fin.read()
        if pairs:
            for start,end in pairs:
                text=remove_all_tags(text,start,end)
        else:
            text=remove_xml_except_title(text)
        fout.write(text)
        fout.close()
        fin.close()
