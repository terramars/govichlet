import numpy
import sys
from scipy.special import gammaln, psi

fin=open(sys.argv[1])
fout=open(sys.argv[2],'w')
nwords=int(sys.argv[3])
ntopics=int(sys.argv[4])

def dirichlet_expectation(alpha):
    return psi(alpha) - psi(numpy.sum(alpha))

matrix=numpy.zeros((nwords,ntopics))

for i in xrange(18):
    fin.readline()

for i in xrange(nwords):
    line=fin.readline()
    line=numpy.array(map(float,line.split()))
    matrix[i,:]=line

for i in xrange(ntopics):
    fout.write(' '.join(map(str,dirichlet_expectation(matrix[:,i])))+'\n')

fout.close()


