from HTMLParser import HTMLParser
from htmlentitydefs import name2codepoint
import sys
import cPickle as cp
import os
import traceback

class TextExtractor(HTMLParser):
    title=''
    active_title=0
    text=''
    useful_stuff={}
    def handle_starttag(self, tag, attrs):
        #print "Start tag:", tag
        #for attr in attrs:
        #    print "     attr:", attr
        if tag=='meta':
            if 'meta' not in self.useful_stuff:
                self.useful_stuff['meta']={}
            if attrs[0][0]=='name' and len(attrs)>1:
                try:
                    self.useful_stuff['meta'][attrs[0][1]]=attrs[1][1]
                except Exception:
                    return
        if tag=='title':
            self.active_title=1
        return
    def handle_endtag(self,tag):
        if tag=='title':
            self.active_title=0
            self.useful_stuff['title']=self.title
        return
    def handle_data(self,data):
        if self.active_title:
            self.title+=data
        else:
            try:
                self.text+=data
            except Exception:
                print data
                raise
        return
    def handle_comment(self,data):
        pass
    def handle_entityref(self, name):
        return
        c = unichr(name2codepoint[name])
        self.text+=c
    def handle_charref(self, name):
        return
        if name.startswith('x'):
            c = unichr(int(name[1:], 16))
        else:
            c = unichr(int(name))
        self.text+=c
    def handle_decl(self, data):
        pass

def parse_folder(infolder,outfolder):
    files=os.listdir(infolder)
    for f in files:
        fin=open(infolder+f)
        parser=TextExtractor()
        try:
            parser.feed(fin.read())
            fout=open(outfolder+f+'.txt','wb')
            fout.write(parser.text)
            fout.close()
            fout=open(outfolder+f+'.ram','w')
            fout.write(cp.dumps(parser.useful_stuff))
        except Exception:
            traceback.print_exc()
            print f
        fout.close()
        fin.close()

if __name__=='__main__':
    infolder=sys.argv[1]
    outfolder=sys.argv[2]
    parse_folder(infolder,outfolder)    
    











