#!/usr/bin/python

# onlinewikipedia.py: Demonstrates the use of online VB for LDA to
# analyze a bunch of random Wikipedia articles.
#
# Copyright (C) 2010  Matthew D. Hoffman
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import cPickle, string, numpy, getopt, sys, random, time, re, pprint

import onlineldavb

    

def main():
    """
    Downloads and analyzes a bunch of random Wikipedia articles using
    online VB for LDA.
    """
    vocab=sys.argv[1]
    docs=sys.argv[2]
    outdir=sys.argv[3]
    # The number of documents to analyze each iteration
    batchsize = 256
    docs = open(docs)
    splitexp=re.compile(r'[ :]')
    ids=[]
    cts=[]
    j=0
    for line in docs:
        j+=1
        try:
            splitline=[i for i in splitexp.split(line)]
        except Exception:
            print line,splitexp.split(line)
            print j
            raise Exception
        wordids = numpy.array([int(i) for i in splitline[0::2]],dtype=numpy.int32)
        wordcts = numpy.array([float(i) for i in splitline[1::2]])
        ids.append(wordids)
        cts.append(wordcts)
    D=len(ids) 
    # The number of topics
    K = 100
    # How many documents to look at
    if (len(sys.argv) < 5):
        documentstoanalyze = int(D/batchsize)
    else:
        documentstoanalyze = int(sys.argv[4])
    print 'running ',documentstoanalyze,' iterations'
    # Our vocabulary
    vocab = [i.strip() for i in file(vocab).readlines()]
    W = len(vocab)
    print D,W
    # Initialize the algorithm with alpha=1/K, eta=1/K, tau_0=1024, kappa=0.7
    olda = onlineldavb.OnlineLDA(W, K, D, 1./K, 1./K, 64., 0.5)
    print olda._expElogbeta.shape
    # Run until we've seen D documents. (Feel free to interrupt *much*
    # sooner than this.)
    flog=open('/mnt/perplexity.log','w')
    idx=range(D)
    for iteration in range(0, documentstoanalyze):
        # Download some articles
    #if 1:
        #tmpidx=random.sample(idx,batchsize)
        tmpidx=idx[iteration*batchsize:(iteration+1)*batchsize]
        tmpids=[ids[i] for i in tmpidx]
        tmpcts=[cts[i] for i in tmpidx]
        # Give them to online LDA
        (gamma, bound) = olda.update_lambda(wordids=tmpids,wordcts=tmpcts)
        # Compute an estimate of held-out perplexity
        #(wordids, wordcts) = onlineldavb.parse_doc_list(docset, olda._vocab)
        perwordbound = bound * len(tmpids) / (D * sum(map(sum, tmpcts)))
        print '%d:  rho_t = %f,  held-out perplexity estimate = %f' % \
            (iteration, olda._rhot, perwordbound)
        flog.write(str(perwordbound)+'\n')
        # Save lambda, the parameters to the variational distributions
        # over topics, and gamma, the parameters to the variational
        # distributions over topic weights for the articles analyzed in
        # the last iteration.
        if (iteration % 10 == 0):
            numpy.savetxt(outdir+'iter-%d.beta' % iteration, olda._Elogbeta)
        #numpy.savetxt(outdir+'gamma-%d.dat' % iteration, gamma)
    (gamma, bound) = olda.update_lambda(wordids=ids,wordcts=cts)
    perwordbound = bound * len(ids) / (D * sum(map(sum, cts)))
    print perwordbound
    numpy.savetxt(outdir+'final.beta', olda._Elogbeta)
    numpy.savetxt(outdir+'final.gamma', gamma)
    
if __name__ == '__main__':
    main()
