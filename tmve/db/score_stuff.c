#include <math.h>

double get_doc_score(double* doca, int na, double* docb, int nb)
{
    double score=0;
    int i=0;
    double thetaa,thetab;
    for (i=0;i!=na;i++)
    {
        thetaa=doca[i];
        thetab=docb[i];
        if (thetaa*thetab!=0.0) score+=pow(thetaa-thetab,2);
    }
    return 0.5*score;
}

double get_topic_score(double* topica, int na, double* topicb, int nb)
{
    double ha=0;
    double hb=0;
    double hab=0;
    double thetaa, thetab;
    int i=0;
    for (i=0;i!=na;i++)
    {
        thetaa=exp(topica[i]);
        thetab=exp(topicb[i]);
        if (thetaa==0.0||thetab==0.0)
            continue;
        ha+=thetaa*topica[i];
        hb+=thetab*topicb[i];
        hab+=log(thetaa+thetab)*(thetaa+thetab);
    }
    return (ha + hb)/(2*hab);
}

double get_topic_score_backup(double* topica, int na, double* topicb, int nb)
{
    double score=0;
    int i=0;
    double total=0;
    double thetaa,thetab;
    total=100.0*na;
    for (i=0;i!=na;i++)
    {
        thetaa=abs(topica[i]);
        thetab=abs(topicb[i]);
        score+=pow(abs(sqrt(thetaa)-sqrt(thetab)),2);
    }
    return 0.5*score/total;
}

double get_term_score(double* terma, int na, double* termb, int nb)
{
    double score=0;
    int i;
    for (i=0;i!=na;i++)
        score+=pow(terma[i]-termb[i],2);
    return score;
}
