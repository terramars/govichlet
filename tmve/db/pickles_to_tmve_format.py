import sys
import cPickle as cp

fin_id=sys.argv[1]
fin_doc=sys.argv[2]
fout_id=sys.argv[3]
fout_doc=sys.argv[4]

ids=cp.load(open(fin_id))
docs=cp.load(open(fin_doc))

ids=ids.items()
ids.sort(key=lambda x: x[1])

fout_id=open(fout_id,'w')

for id,_ in ids:
    fout_id.write(id+'\n')

fout_id.close()

docs=docs.items()
docs.sort()

fout_doc=open(fout_doc,'w')

for _,doc in docs:
    doc=doc.rsplit('/',3)
    doc=doc[1]+' '+doc[2]+' '+doc[3][:-4]
    fout_doc.write(doc+'\n')

fout_doc.close()
