extern double get_doc_score(double* doca, int na, double* docb, int nb);
extern double get_topic_score(double* topica, int na, double* topicb, int nb);
extern double get_term_score(double* terma, int na, double* termb, int nb);
