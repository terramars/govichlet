%module score_stuff
%{
    /* Includes the header in the wrapper code */
    #define SWIG_FILE_WITH_INIT
    #include "score_stuff.h"
%}

%include "numpy.i"
%init %{
    import_array();
%}
/* Parse the header file to generate wrappers */
%apply (double* INPLACE_ARRAY1, int DIM1) {(double* doca, int na),(double* docb, int nb),(double* topica, int na),(double* topicb, int nb),(double* terma, int na),(double* termb, int nb)}

%include "score_stuff.h"
