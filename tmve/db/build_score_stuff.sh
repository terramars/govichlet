swig -python score_stuff.i
gcc -O3 -fPIC -c score_stuff.c -I/usr/include/python2.7 -I/usr/include/python2.7/numpy
gcc -O3 -fPIC -c score_stuff_wrap.c -I/usr/include/python2.7 -I/usr/include/python2.7/numpy
gcc -shared *.o -o _score_stuff.so
rm score_stuff*.o score_stuff_wrap.c
