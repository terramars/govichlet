float damping = 0.99;
int delt = 4;
int window = 0;
float popRate = 0.035;
float dt = 1/(float)delt;
float gravity = -1;
int counter = 0;
ArrayList<ball> blist;
binA bins;
ArrayList<topic> topics;

void setup()
{
	topics = new ArrayList();
	size(700,500);
	bins = new binA(11 * 4,5 * 4);
	blist = new ArrayList();
	/*
	for (int i = 0;i<100;i++)
	{
		ball t = new ball(random(width), random(height), pow(random(7,9),1));
		blist.add(t);
	}
	*/
}

void draw()
{
	background(243);
	for (int d = 0; d<delt;d++)
	{
		bins.place(blist);
		bins.collide(blist);
		for (int i = 0;i<blist.size();i++)
		{
			ball a = blist.get(i);
			float dx,dy,dis;
			dx = a.x - width/2;
			dy = a.y - height/2;
			//dx = a.x - mouseX;
			//dy = a.y - mouseY;
			
			dis = sqrt(dx*dx+dy*dy);
			if (dis > 0)
			{
				a.xa += dx/dis * gravity;
				a.ya += dy/dis * gravity;
			}
			/*
			for (int j = 0;j<blist.size();j++)
			{
				if (i != j)
				{
					ball b = blist.get(j);
					dx = a.x - b.x;
					dy = a.y - b.y;
					dis = sqrt(dx*dx+dy*dy);
					if (dis > 0)
					{
						if (a.t.equals(b.t))
						{
							a.xa -= dx/dis * 0.5;
							a.ya -= dy/dis * 0.5;
							b.xa += dx/dis * 0.5;
							b.ya += dy/dis * 0.5;
						}
					}
				}
			}
			*/
			a.update(dt);
		}
		bins.empty();
	}
	for (int i = 0;i<blist.size();i++)
	{
		ball a = blist.get(i);
		float dx,dy,dis;
		dx = a.x - mouseX;
		dy = a.y - mouseY;
		dis = sqrt(dx*dx+dy*dy);
		
		if (dis < a.r && mousePressed)
		{
			if (abs(a.r - 80) < abs(a.r - a.r0))
			{
				a.tr = a.r0;
			}
			else
			{
				a.tr = 80;
			}
		}
		
	}
	fill(255,0,0);
	noStroke();
	//noFill();
	//stroke(255,0,0);
	for (int i = 0;i<blist.size();i++)
	{
		ball a = blist.get(i);
		a.draw();
	}
}

void addTopic(float score)
{
	topic t = new topic();
	t.score = score;
	topics.add(t);
	//println(score);
	
}
void addTopicAssoc(int id, float val, String word)
{
	topic t = topics.get(id);
	t.aScore.add(val);
	t.assoc.add(word);
	//if (counter == 0)
	//{
		ball b = new ball(random(width), random(height), 16 + random(-6,6));
		b.label = word;
		b.t = t;
		blist.add(b);
	//}
	//counter = (counter + 1) % 50;

}


class ball
{
	float x,y,x0,y0,xa,ya;
	float r;
	float tr;
	float r0;
	String label;
	topic t;
	ball(float x, float y, float r)
	{
		this.x = x;
		x0 = x;
		this.y = y;
		y0 = y;
		this.r = r;
		tr = r;
		r0 = r;
	}
	void update(float dt)
	{
		float tx,ty;
		tx = x;
		ty = y;
		x = x + (x - x0) * damping + xa * dt * dt;
		y = y + (y - y0) * damping + ya * dt * dt;
		x = constrain(x,r,width - r);
		y = constrain(y,r,height - r);
		r = r * (1 - popRate) + tr * popRate;
		x0 = tx;
		y0 = ty;
		xa = 0;
		ya = 0;
		
	}
	void draw()
	{
		fill(t.c);
		ellipse(x,y,r * 2,r * 2);
		fill(255);
		text(label,x-textWidth(label)/2,y);
	}
	void collide(ball b)
	{
		float dx,dy,dis;
		dx = x - b.x;
		dy = y - b.y;
		dis = sqrt(dx * dx + dy * dy);
		if (dis < r + b.r && dis > 0)
		{
			float ovr = -(dis - (r + b.r)) * 0.5;
			x += dx/dis * ovr;
			y += dy/dis * ovr;
			b.x -= dx/dis * ovr;
			b.y -= dy/dis * ovr;
		}
	}
}

class binA
{
	ArrayList<ball>[][] binArr;
	int bw;
	int bh;
	binA(int binw, int binh)
	{
		bw = binw;
		bh = binh;
		binArr = new ArrayList[binw][binh];
		for (int i = 0;i < binw; i++)
		{
			for (int j = 0;j < binh; j++)
			{
				binArr[i][j] = new ArrayList<ball>();
			}
		}
	}
	void place(ArrayList<ball> b)
	{
		for (int i = 0;i < b.size(); i++)
		{
			int umin = (int)constrain((b.get(i).x / bw) - (b.get(i).r * 2)/bw,0,bw-1);
			int vmin = (int)constrain((b.get(i).y / bh) - (b.get(i).r * 2)/bh,0,bh-1);
			int umax = (int)constrain((b.get(i).x / bw) + (b.get(i).r * 2)/bw,0,bw-1);
			int vmax = (int)constrain((b.get(i).y / bh) + (b.get(i).r * 2)/bh,0,bh-1);
			for (int u = umin; u<umax + 1;u++)
			{
				for (int v = vmin; v<vmax + 1;v++)
				{
					binArr[u][v].add(b.get(i));
				}
			}
		}
	}
	void collide(ArrayList<ball> bl)
	{
		int umin;
		int vmin;
		int umax;
		int vmax;
		
		for (int i = 0;i < bl.size(); i++)
		{
			ball a = bl.get(i);
			umin = (int)constrain((bl.get(i).x / bw) - window,0,bw-1);
			vmin = (int)constrain((bl.get(i).y / bh) - window,0,bh-1);
			umax = constrain(umin + 2 * window,0,bw-1);
			vmax = constrain(vmin + 2 * window,0,bh-1);
			for (int u = umin; u<umax + 1;u++)
			{
				for (int v = vmin; v<vmax + 1;v++)
				{
					for (int j = 0;j < binArr[u][v].size(); j++)
					{
						ball b = binArr[u][v].get(j);
						if (!a.equals(b))
						{
							a.collide(b);
						}
					}
				}
			}
		}
	}
	void empty()
	{
		for (int i = 0;i < bw; i++)
		{
			for (int j = 0;j < bh; j++)
			{
				binArr[i][j].clear();
			}
		}
	}
}

class topic
{
	float score;
	ArrayList<String> assoc;
	ArrayList<Float> aScore;
	color c;
	topic()
	{
		c = color(random(255),random(255),random(255));
		score = 0;
		assoc = new ArrayList();
		aScore = new ArrayList();
	}
}

void mouseClicked()
{
	
}