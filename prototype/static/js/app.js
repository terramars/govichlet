//Templates
window.JST = {};

window.JST['doc_sidebar'] = _.template(
    '<%= topics %>' +
    '<%= docs %>'
);

window.JST['topic_sidebar'] = _.template(
    '<%= terms %>'
);


window.JST['doc_sidebar/topicList'] = _.template(
    '<h2>Topics:</h2>' +
    '<% _.each(topics, function(topic, i){ %>' +
        '<%= topic %>' +
    '<% }); %>'
);

window.JST['topic_sidebar/termList'] = _.template(
    '<h2>Terms:</h2>' +
    '<% _.each(terms, function(term, i){ %>' +
        '<%= term %>' +
    '<% }); %>'
);

window.JST['topic/docList'] = _.template(
    '<h2>Similar documents:</h2>' + 
    '<ol>' +
        '<% _.each(docs, function(doc, i){ %>' +
            '<li><%= doc %></li>' +
        '<% }); %>' +
    '</ol>'
);

window.JST['doc_sidebar/docList'] = _.template(
    '<h2>Similar documents:</h2>' +
    '<% _.each(docs, function(doc, i){ %>' +
        '<%= doc %>' +
    '<% }); %>'
);

window.JST['doc_sidebar/docStatus'] = _.template(
    '<ul>' +
        '<li>Congress: <%= congress %></li>' +
        '<li>Status: <%= bill_status %></li>' +
    '</ul>'
);

window.JST['doc_sidebar/topic'] = _.template(
    '<% var w = Math.min(100.0, Math.round(score * 100.0)) %>' +
    '<a href="/demo/topic/<%= topic_id %>/">' + 
        '<span>' +
            '<%= topic %>' +
            '<div class="progress progress-success">' +
                '<div class="bar" style="width: <%= w %>%"</div>' +
            '</div>' +
        '</span>' +
    '</a>'
);

window.JST['topic_sidebar/term'] = _.template(
    '<% var w = Math.min(100.0, Math.round(score * 100.0)) %>' +
    '<a href="#">' + 
        '<span>' +
            '<%= term %>' +
            '<div class="progress progress-success">' +
                '<div class="bar" style="width: <%= w %>%"</div>' +
            '</div>' +
        '</span>' +
    '</a>'
);

window.JST['topic/similar_doc'] = _.template(
    '<a href="/demo/doc/<%= doc_id %>/">' +
        '<%= title %>' +
    '</a>'
);

window.JST['doc_sidebar/similar_doc'] = _.template(
    '<% var w = Math.min(100.0, Math.round(score * 100.0)) %>' +
    '<a href="/demo/doc/<%= doc_id %>/">' +
        '<span>' +
            '<%= title %>' +
            '<div class="progress progress-success">' +
                '<div class="bar" style="width: <%= w %>%"</div>' +
            '</div>' +
        '</span>' +
    '</a>'
);

window.JST['doc'] = _.template(
    '<h1><%= title %></h1>' +
    '<div><%= text %></div>'
);

window.JST['topic'] = _.template(
    '<h1><%= topic.topic %></h1>' +
    '<%= docs %>'
);

(function($, Backbone, _){
    //
    $.fn.outerHTML = function() {
        return $(this).clone().wrap('<p>').parent().html();
    }

    //Models
    var Term = Backbone.Model.extend({
        defaults: {
            term_id: null,
            term: null,
            score: null
        }
    });

    var Topic = Backbone.Model.extend({
        defaults: {
            topic_id: null,
            topic: null,
            score: null,
            terms: null,
            similar_docs: null
        }
    });

    var Document = Backbone.Model.extend({
        defaults: {
            id: null,
            title: null,
            text: null,
            similar_docs: [],
            congress: null,
            bill_status: null
        }
    });

    //Collections
    var TermList = Backbone.Collection.extend({
        model: Term
    });

    var TopicList = Backbone.Collection.extend({
        model: Topic
    });

    var SimilarDocList = Backbone.Collection.extend({
        model: Document
    });

    //Views
    var TopicView = Backbone.View.extend({
        tagName: 'div',
        className: 'topic',
        initialize: function() {
            _.bindAll(this, 'render');
        },
        render: function() {
            this.$el.html(JST['doc_sidebar/topic'](this.model));
            return this;
        }
    });

    var TermView = Backbone.View.extend({
        tagName: 'div',
        className: 'term',
        initialize: function() {
            _.bindAll(this, 'render');
        },
        render: function() {
            this.$el.html(JST['topic_sidebar/term'](this.model));
            return this;
        }
    });

    var TopicListView = Backbone.View.extend({
        tagName: 'div',
        className: 'topics',

        initialize: function(models) {
            _.bindAll(this, 'render');
            this.collection = new TopicList(models);
        },

        render: function() {
            var self = this;
            var topics = []
            _.each(this.collection.models, function(topic, i) {
                topicView = new TopicView({
                    model: topic.toJSON()
                });
                topics.push(topicView.render().$el.outerHTML());
            });
            this.$el.html(JST['doc_sidebar/topicList']({
                'topics': topics
            }));
            return this;
        }
    });

    var TermListView = Backbone.View.extend({
        tagName: 'div',
        className: 'terms',

        initialize: function(models) {
            _.bindAll(this, 'render');
            this.collection = new TermList(models);
        },

        render: function() {
            var self = this;
            var terms = []
            _.each(this.collection.models, function(term, i) {
                termView = new TermView({
                    model: term.toJSON()
                });
                terms.push(termView.render().$el.outerHTML());
            });
            this.$el.html(JST['topic_sidebar/termList']({
                'terms': terms
            }));
            return this;
        }
    });

    var SimilarDocView = Backbone.View.extend({
        tagName: 'div',
        className: 'similar-doc',
        initialize: function(options) {
            _.bindAll(this, 'render');
        },
        render: function() {
            console.log('rendering similar doc', this.el);
            this.$el.html(JST[this.options.template](this.model));
            return this;
        }
    });

    var SimilarDocListView = Backbone.View.extend({
        tagName: 'div',
        className: 'similar-docs',

        initialize: function(models, template, similar_doc_template) {
            _.bindAll(this, 'render');
            this.collection = new SimilarDocList(models);
            this.template = template;
            this.similar_doc_template = similar_doc_template;
        },

        render: function() {
            var self = this;
            var docs = []
            _.each(this.collection.models, function(doc, i) {
                docView = new SimilarDocView({
                    model: doc.toJSON(),
                    template: self.similar_doc_template
                });
                docs.push(docView.render().$el.outerHTML());
            });
            this.$el.html(JST[this.template]({
                'docs': docs
            }));
            return this;
        }
    });

    var MainDocumentView = Backbone.View.extend({
        tagName: 'div',
        className: 'main-doc',

        initialize: function() {
            _.bindAll(this, 'render');
        },

        render: function() {
             this.$el.html(JST['doc'](this.model));
             return this;
        }
    });

    var DocumentMetadataView = Backbone.View.extend({
        tagName: 'div',
        className: 'metadata-pane',

        initialize: function() {
            _.bindAll(this, 'render');
        },

        render: function() {
            topicListView = new TopicListView(this.model.topics);
            var topic_list_rendered = topicListView.render().$el.outerHTML();
            similarDocListView = new SimilarDocListView(
                this.model.similar_docs,
                'doc_sidebar/docList',
                'doc_sidebar/similar_doc'
            );
            var doc_list_rendered = similarDocListView.render().$el.outerHTML();
            this.$el.html(JST['doc_sidebar']({
                'topics': topic_list_rendered,
                'docs': doc_list_rendered
            }));
            return this;
        }
    });

    var DocumentStatusView = Backbone.View.extend({
        tagName: 'div',
        className: 'status-pane',

        initialize: function() {
            _.bindAll(this, 'render');
        },

        render: function() {
           this.$el.html(JST['doc_sidebar/docStatus']({
                congress: this.model.congress,
                bill_status: this.model.bill_status
           }));
           return this;
        }
    });

    var MainTopicView = Backbone.View.extend({
        tagName: 'div',
        className: '',
        initialize: function() {
            _.bindAll(this, 'render');
        },

        render: function() {
            similarDocListView = new SimilarDocListView(
                this.model.similar_docs,
                'topic/docList',
                'topic/similar_doc'
            );
            var doc_list_rendered = similarDocListView.render().$el.outerHTML();
            this.$el.html(JST['topic']({
                'topic': this.model,
                'docs': doc_list_rendered
            }));

            return this;
        }

    });

    var TopicMetadataView = Backbone.View.extend({
        tagName: 'div',
        className: 'metadata-pane',

        initialize: function() {
            _.bindAll(this, 'render');
        },

        render: function() {
            termListView = new TermListView(this.model.terms);
            var term_list_rendered = termListView.render().$el.outerHTML();
            this.$el.html(JST['topic_sidebar']({
                'terms': term_list_rendered
            }));
            return this;
        }

    });

    var AppView = Backbone.View.extend({
        el: $('body'),
        left_pane: $('#left-pane'),
        main_pane: $('#main-pane'),
        right_pane: $('#right-pane'),

        events: {
        },

        initialize: function(leftViewClass, mainViewClass, rightViewClass) {
            _.bindAll(this, 'render', 'renderLeftView', 'renderMainView');
            this.leftViewClass = leftViewClass;
            this.mainViewClass = mainViewClass;
            this.rightViewClass = rightViewClass;
            //$(window).bind('resize.app', _.bind(this.resize, this));
            this.render();
            //this.resize();
        },

        renderLeftView: function(model) {
            leftDocView = new this.leftViewClass({
                model: model
            });
            this.left_pane.html(leftDocView.render().$el.outerHTML())
        },

        renderRightView: function(model) {
            if (this.rightViewClass) {
                rightDocView = new this.rightViewClass({
                    model: model
                });
                this.right_pane.html(rightDocView.render().$el.outerHTML())
            }
        },

        renderMainView: function(model) {
            mainDocView = new this.mainViewClass({
                model: model
            });
            this.main_pane.html(mainDocView.render().$el.outerHTML())
        },

        render: function() {
            var self = this;
            this.renderMainView(this.model);
            this.renderLeftView(this.model);
            this.renderRightView(this.model);
        },

        resize: function() {
            var height = ($(window).height() - $('#top-toolbar').height());
            $.each([this.main_pane, this.left_pane, this.right_pane], function(i, pane) {
                var container = pane.parent();
                container.height(height - parseInt((container.css('padding-top')) + parseInt(container.css('padding-bottom')) + parseInt(container.css('border-width'))*2));
            });
        }
    });

    var DocAppView = AppView.extend({
        initialize: function() {
            this.constructor.__super__.initialize.apply(
                this, 
                [DocumentMetadataView, MainDocumentView, DocumentStatusView]
            );
        }
    });

    var TopicAppView = AppView.extend({
        initialize: function() {
            this.constructor.__super__.initialize.apply(
                this, 
                [TopicMetadataView, MainTopicView]
            );
        }
    });

    window.start_docview = function(main_doc) {
        var docapp = new DocAppView({
            model: main_doc
        });
    };
    window.start_topicview = function(topic) {
        var topicapp = new TopicAppView({
            model: topic
        });
    };

})(jQuery, Backbone, _);
