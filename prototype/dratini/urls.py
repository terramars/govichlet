from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('dratini.views',
    url(r'^doc/(?P<doc_id>\d+)/$', 'get_doc'),
    url(r'^topic/(?P<topic_id>\d+)/$', 'get_topic'),
    url(r'^test/$', 'test'),
)
