from django.db import models
from djangotoolbox.fields import ListField

# Create your models here.
class Term(models.Model):
    _id = models.CharField(db_column="_id", primary_key=True, max_length=200)
    word = models.CharField(max_length = 200)
    word_count = models.PositiveIntegerField(null=True)
    doc_count = models.PositiveIntegerField(null=True)

    class Meta:
        db_table = 'terms'

class Document(models.Model):
    _id = models.CharField(db_column="_id", primary_key=True, max_length=200)
    path = models.CharField(max_length=200)
    title = models.CharField(max_length=200)
    text = models.TextField()
    readable_title = models.CharField(max_length=500)
    topics = ListField()
    wordids = ListField()
    wordcts = ListField()
    similar_docs = ListField()
    doc_scores = ListField()

    class Meta:
        abstract = True

class GovDocument(Document):
    status = models.CharField(max_length=200)
    congress = models.CharField(max_length=200)

    class Meta:
        db_table = 'docs'

class Topic(models.Model):
    _id = models.CharField(db_column="_id", primary_key=True, max_length=200)
    terms = ListField()
    scores = ListField()
    similar_docs = ListField()

    class Meta:
        db_table = 'topics'
