import re
import cgi

re_string = re.compile(r'(?P<htmlchars>[<&>])|(?P<space>^[ \t]+)|(?P<lineend>\r\n|\r|\n)|(?P<protocal>(^|\s)((http|ftp)://.*?))(\s|$)', re.S|re.M|re.I)
def plaintext2html(text, tabstop=4):
    def do_sub(m):
        c = m.groupdict()
        if c['htmlchars']:
            return cgi.escape(c['htmlchars'])
        if c['lineend']:
            return '<br />'
        elif c['space']:
            t = m.group().replace('\t', '&nbsp;'*tabstop)
            t = t.replace(' ', '&nbsp;')
            return t
        elif c['space'] == '\t':
            return ' '*tabstop;
        else:
            url = m.group('protocal')
            if url.startswith(' '):
                prefix = ' '
                url = url[1:]
            else:
                prefix = ''
            last = m.groups()[-1]
            if last in ['\n', '\r', '\r\n']:
                last = '<br />'
            return '%s<a href="%s">%s</a>%s' % (prefix, url, url, last)
    return re.sub(re_string, do_sub, text)


date_regex = re.compile(r'(\d+\/\d+\/\d+--)(.*)')
section_regex = re.compile(r'(\(Sec\. \d+\))([^.]+\.)')
def format_doc_text(text):
    text = date_regex.sub(lambda matchobj: '<h3>%s</h3><p>%s</p>' % (matchobj.group(1), matchobj.group(2)), text, count=0)
    text = section_regex.sub(lambda matchobj: '<li><i>%s</i>%s</li>' % (matchobj.group(1), matchobj.group(2)), text)
    return text
