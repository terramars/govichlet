from django.shortcuts import render_to_response
from django.http import HttpResponse, Http404
from django.shortcuts import get_object_or_404
from django.utils import simplejson
from django.template import RequestContext
from dratini.models import Term, GovDocument, Topic
from django.core.context_processors import csrf
import formatter

def index(request):
    return HttpResponse()

def normList(L, normalizeTo=1):
    '''normalize values of a list to make its max = normalizeTo'''
    vMax = max(L)
    return [ x/(vMax*1.0)*normalizeTo for x in L]

def get_doc_data(document):
    data = {}
    data['doc_id'] = document._id
    data['text'] = formatter.format_doc_text(document.text)
    data['title'] = document.readable_title
    data['congress'] = document.congress
    data['bill_status'] = document.status
    data['similar_docs'] = get_doc_docs(document)
    data['topics'] = get_doc_topics(document)
    return data

def get_topic_data(topic):
    data = {}
    data['topic'] = get_topic_words(topic)
    data['topic_id'] = str(topic.pk)
    data['terms'] = get_topic_terms(topic)
    data['similar_docs'] = get_topic_docs(topic)
    return data

def get_topic_words(topic, num_words=3):
    words = []
    for word_id in topic.terms[:3]:
        words.append(Term.objects.get(pk=str(word_id)).word)
    return '{%s}' % ', '.join(words)

def get_topic_terms(topic, num_terms=10):
    '''
    returns a sorted list of dicts:
    {term: term_name, score: likelihood_score, term_id: term id}
    '''
    term_ids = topic.terms[:num_terms]
    term_names = [Term.objects.get(pk=str(i)).word for i in term_ids]
    scores = normList(topic.scores)
    terms_scores = [{'term': t, 'score': s, 'term_id': i} for i,t,s in zip(term_ids, term_names, scores)]
    return terms_scores

def get_topic_docs(topic, num_docs=100):
    docs = []
    for doc_id in topic.similar_docs[:100]:
        doc = GovDocument.objects.get(pk=str(doc_id))
        docs.append({
            'doc_id': doc._id,
            'title': doc.readable_title,
        })
    return docs

def get_doc_topics(document, num_topics=5, topic_threshold=0.05):
    top_topics = sorted(enumerate(document.topics), key = lambda x: x[1], reverse=True)
    min_val=top_topics[-1][1]
    top_topics=[i for i in top_topics if i[1]-min_val>topic_threshold]

    topics_scores = []
    for topic_id, score in top_topics:
        topic = Topic.objects.get(pk=str(topic_id))
        words = get_topic_words(topic)
        topics_scores.append({'topic': words, 'score': score, 'topic_id': topic_id})
    return topics_scores

def get_doc_docs(document, doc_threshold = .3):
    docs_scores = sorted(filter(lambda x: x[1] > doc_threshold, zip(document.similar_docs, document.doc_scores)), key = lambda x: x[1], reverse=True)
    docs_data = []
    for doc_id, score in docs_scores:
        related_doc = GovDocument.objects.get(pk=str(doc_id))
        datum = {
            'doc_id': related_doc._id,
            'title': related_doc.readable_title,
            'score': score
        }
        docs_data.append(datum)
    return docs_data

def get_doc(request, doc_id):
    doc = get_object_or_404(GovDocument, pk=doc_id)
    doc_data = simplejson.dumps(get_doc_data(doc))
    ret = {'data': doc_data}
    ret.update(csrf(request))
    return render_to_response(
        'docview.html',
        ret,
        context_instance = RequestContext(request))

def get_topic(request, topic_id):
    topic = get_object_or_404(Topic, pk=str(topic_id))
    topic_data = simplejson.dumps(get_topic_data(topic))
    ret = {'data': topic_data}
    ret.update(csrf(request))
    return render_to_response(
        'topicview.html',
        ret,
        context_instance = RequestContext(request))


def test(request):
    documents = GovDocument.objects.all()[:1000]
    return render_to_response(
        'test.html',
        {},
        context_instance = RequestContext(request))
