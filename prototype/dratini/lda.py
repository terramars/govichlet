import os
from dratini.models import Document, Term 
import tempfile
import subprocess

def prepare_for_vowpal_wabbit(documents):
    target_file = tempfile.NamedTemporaryFile(delete=False)
    for document in documents:
        data_line = '| ' + ' '.join('%s:%s' % d for d in zip(document.wordids, document.wordcts))
        target_file.write(data_line)
        target_file.write('\n')
    print target_file.name
    return target_file

def run_vowpal_wabbit(documents, num_topics=10, lda_alpha=.01, lda_rho=.01, minibatch=128, power_t=.5, initial_t=1, b=16, cache_file='/tmp/vw.cache', passes=1):
    '''
    Run vowpal wabbit on a queryset of Documents
    '''
    data_file = prepare_for_vowpal_wabbit(documents)
    predictions_file = '/tmp/vw.predictions.dat'
    readable_model_file = '/tmp/vw.topics.dat'
    vw_args = [
        'vw',
        data_file.name,
        '--lda', str(num_topics), 
        '--lda_D', str(documents.count()),
        '--lda_alpha', str(lda_alpha),
        '--lda_rho', str(lda_rho),
        '--minibatch', str(minibatch),
        '--power_t', str(power_t),
        '--initial_t', str(initial_t),
        '-b', str(b),
        '--cache_file', cache_file,
        '-p', str(predictions_file),
        '--readable_model', str(readable_model_file),
    ]
    print ' '.join(vw_args)
    subprocess.call(vw_args)
    data_file.close()

def get_topics(documents):
    pass
