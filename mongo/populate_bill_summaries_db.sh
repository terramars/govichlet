#!/bin/bash
DIR="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
python "${DIR}/write_bill_summaries_list.py" "bill-summaries" "/root/summaries/files.txt"
python "${DIR}/write_word_list.py" "bill-summaries" "/root/summaries/words.txt"
python "${DIR}/write_id_list.py" "bill-summaries" "/root/summaries/ids.txt"
