from xml.etree import cElementTree as etree
import sys
import mongo_corpus as mc
import os

class Document:
    def __init__(self,doc_id,title,base_path=None):
        self.id=doc_id
        self.title=str(title)
        self.file_title=self.title
        if base_path:
            self.file_title=base_path+self.title
        self.readable_title=self.title

    def get_safe_title(self):
        safe_title = self.title.replace(" ", "_").replace("\'", "_").replace("/", "-")
        return safe_title

    def get_display(self):
        doc=open(self.file_title).read()
        return doc

    def get_mongo_format(self):
        return mc.format_doc(self.id,self.file_title,self.title,self.readable_title)

class BillSummaryDocument(Document):
    def __init__(self,doc_id,title,base_path,control_char='/',file_extension='.txt'):
        self.id=doc_id
        self.title=str(title)
        congress,status,number=title.split(control_char)
        location=base_path+congress+'/'+status+'/'+number
        self.file_title=location+file_extension
        root=etree.parse(location+'.xml')
        root=root.find('titles')
        titles=root.findall('title')
        short=[i for i in titles if i.get('type')=='short']
        best=[i for i in short if i.get('as')=='enacted']
        official=[i for i in titles if i.get('type')=='official']
        if len(official):
            title=official[0].text
        elif len(best):
            title=best[0].text
        elif len(short):
            title=short[0].text
        else:
            title=titles[0].text
        self.readable_title=title
        titles=list(set([i.text for i in titles if not i.text==title]))
        extra_data={}
        if len(titles):
            extra_data['alternate_titles']=titles
        extra_data['congress']=int(congress)
        extra_data['status']=status
        extra_data['number']=number
        extra_data['text']=open(self.file_title).read()
        self.extra_data=extra_data
    
    def get_mongo_format(self):
        return mc.format_doc(self.id,self.file_title,self.title,self.readable_title,meta=self.extra_data)
