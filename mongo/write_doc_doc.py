from mongo_corpus import *
import sys
from itertools import imap

import numpy as np
from numpy import argsort, asarray

from distance_metrics import kl_distance

def get_doc_relations(a,b,a_log=None,cached_log=None):
    return kl_distance(a,b,a_log=a_log,cached_log=cached_log)

def get_top_docs(db,n=50):
    docs=get_doc_topic_matrix(db)
    docs=np.abs(docs.T/np.sum(docs.T,axis=0)).T
    print docs.shape
    log_docs=np.log(docs)
    log_docs=np.choose((log_docs<-1000),(log_docs,-1000))
    log_docs=np.choose((log_docs>1000),(log_docs,1000))
    #relations=np.zeros((docs.shape[0],docs.shape[0]),dtype=np.float32)
    for i in xrange(docs.shape[0]-1):
        #scores=get_doc_relations(docs[i,:],docs[i+1,:])
        scores=get_doc_relations(docs[i,:],log_docs,a_log=log_docs[i,:],cached_log=True)
        #relations[i,i+1:]=scores
        #relations[i+1:,i]=scores
       # for i in xrange(docs.shape[0]):
        #to_compare=relations[i,:]
        to_compare=np.abs(scores)
        #if i%5000==0:
        #    print 'writing relations',i
        ids=argsort(to_compare)[0:n+1]    
        if i%1000==0:
            print 'found scores',i,ids,scores[ids]
        update_doc_doc(db,i,ids,to_compare[ids])
    return


if __name__=='__main__':
    name=sys.argv[1]
    if len(sys.argv)>4:
        host=sys.argv[2]
        port=sys.argv[3]
        db=connect(host,port)
    else:
        db=connect()
    db=load_db(db,name)
    get_top_docs(db) 

