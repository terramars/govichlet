from mongo_corpus import *
import sys
import os
from document_tools import *

def load_doc_list_to_ids(fin):
    ids={}
    i=0
    for line in open(fin):
        ids[i]=line.strip()
        i+=1
    return ids

def write_docs_to_mongo(db,ids,base_path):
    docs=[]
    for i,title in ids.items():
        try:
            doc=BillSummaryDocument(i,title,base_path)
        except Exception:
            print i,title
        docs.append(doc.get_mongo_format())
        if len(docs) and i%1000==0:
            print i
            insert_doc_list(db,docs)
            docs=[]
    if len(docs):
        insert_doc_list(db,docs)
    return


if __name__=='__main__':
    name=sys.argv[1]
    fin=sys.argv[2]
    base_path=os.path.dirname(fin)+'/'
    if len(sys.argv)>5:
        host=sys.argv[3]
        port=sys.argv[4]
        db=connect(host,port)
    else:
        db=connect()
    db=load_db(db,name)
    ids=load_doc_list_to_ids(fin)
    write_docs_to_mongo(db,ids,base_path)    
