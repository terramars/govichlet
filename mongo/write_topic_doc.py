import mongo_corpus
import numpy
import logging

FORMAT = '%(asctime)s %(funcName)s: %(message)s'
logging.basicConfig(format=FORMAT, level=logging.DEBUG)

def write_topic_docs(db, num_docs):
    logging.debug('loading from mongodb.. grabbing all docs into memory')
    doc_topic_matrix = mongo_corpus.get_doc_topic_matrix(db)
    logging.debug('loaded doc topic matrix')
    for col in xrange(doc_topic_matrix.shape[1]):
        topic_id = str(col)
        top_docs = numpy.argsort(doc_topic_matrix[:,col])[::-1][:num_docs]
	if col%20==0:
	    print top_docs[:10],doc_topic_matrix[:,col][top_docs[:10]]
        topic_doc_ids = map(str, top_docs)
        logging.debug('found top %d docs for topic %s' % (num_docs, topic_id))
        mongo_corpus.update_topic_docs(db, topic_id, topic_doc_ids)
        logging.debug('wrote to mongo for topic %s' % (topic_id,))

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Write topic-doc into mongodb')
    parser.add_argument('--num-docs', action="store", type=int, default=1000)
    parser.add_argument('--db', action="store", default='bill-summaries', type=str)
    parser.add_argument('--host', action="store", default='localhost', type=str)
    parser.add_argument('--port', action="store", default='27017', type=str)
 
    args = parser.parse_args()
    connection = mongo_corpus.connect(args.host, args.port)
    db = mongo_corpus.load_db(connection, args.db)
    write_topic_docs(db, args.num_docs)
