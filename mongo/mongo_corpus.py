from pymongo import Connection
import cPickle as cp
import numpy as np
import sys

def connect(host='localhost',port='27017'):
    connection=Connection(host,int(port))
    return connection

def load_db(connection,db):
    return connection[db]

def initialize_index(db,collection,field,unique=True):
    coll=db[collection]
    coll.ensure_index(field,unique=unique)

def format_term(word,idx,counts=(0,0)):
    term=   {'word':word,
             '_id':str(idx),
             'word_count':counts[0],
             'doc_count':counts[1]}
    return term

def format_doc(idx,file_path=None,title=None,readable_title=None,wordids=None,wordcts=None,meta=None):
    doc={'_id':str(idx)}
    if file_path:
        doc['path']=file_path
    if title:
        doc['title']=title
    if readable_title:
        doc['readable_title']=readable_title
    if wordids:
        doc['wordids']=wordids
        doc['wordcts']=wordcts
        if not wordcts:
            raise Exception
    if meta and type(meta)==type({}):
        for key,val in meta.items():
            doc[key]=val
    return doc

# unlike words, we want to be able to change docs - in case our vocab changes or just for convenience so we can add in batches
# as such, we assume our inputs are already formatted and use .save so we handle updates
def save_doc_list(db,docs):
    docs=[doc for doc in docs if '_id' in doc]
    i=0
    for doc in docs:
        db.docs.save(doc)
        i+=1
        if i%4000==0:
            print i
    return

def add_ids_and_cts_to_docs(db, docs):
    docs=[doc for doc in docs if '_id' in doc]
    i=0
    for doc in docs:
        db.docs.update({'_id': doc['_id']},{'$set':{'wordids':doc['wordids'],'wordcts':doc['wordcts']}})
        i+=1
        if i%1000==0:
            print i
    return

#vectorized doc stuff for insertion
def insert_doc_list(db,docs):
    docs=[doc for doc in docs if '_id' in doc]
    db.docs.insert(docs)
    return

# fill in vocab off of a list of words - indexes are assumed to be monotonically increasing starting from the given i
# be careful with alignment!!
def insert_word_list(db,words,i=None):
    ids={}
    terms=[]
    if not i:
        i=db.terms.count()-1
    if i==-1:
        i=0
        db.terms.insert({'_id':'-1','word':'collection_stats','word_count':0,'doc_count':0})
    for word in words:
        if not db.terms.find_one({'word':word}):
            term=format_term(word,i)
            ids[i]=word
            terms.append(term)
            i+=1    
            if i%1000==0:    
                db.terms.insert(terms)
                terms=[]
    if len(terms):
        db.terms.insert(terms)
    return ids,i # returns the id:word map that we've just added, as well as the next value of i.  this will allow merges on term lists.

# this assumes that you've already loaded all of the words in correctly
def update_word_counts(db,wordids,wordcts,docts,ndocs=1):
    for i in xrange(len(wordids)):
        db.terms.update({'_id':str(wordids[i])},{'$inc':{'word_count':wordcts[i],'doc_count':docts[i]}})
        db.terms.update({'_id':'-1'},{'$inc':{'word_count':wordcts[i]}})
    db.terms.update({'_id':'-1'},{'$inc':{'doc_count':ndocs}})
    return

def update_doc_topics(db, doc_idx, topics):
    db.docs.update(
        {'_id': str(doc_idx)},
        {'$set': {'topics': topics}})

def update_doc_doc(db,doc_idx,docs,scores):
    db.docs.update({'_id':str(doc_idx)},{'$set': {'similar_docs':map(str,docs),'doc_scores':map(float,scores)}})
    return

def update_term_term(db,term_idx,terms,scores):
    db.terms.update({'_id':str(term_idx)},{'$set': {'similar_terms':map(str,terms),'term_scores':map(float,scores)}})

def update_topic_topic(db,topic_idx,topics,topic_scores):
    db.topics.update({'_id':str(topic_idx)},{'$set': {'similar_topics':map(str,topics),'topic_scores':map(float,topic_scores)}})

def get_doc_topic_matrix(db):
    docs=np.zeros((db.docs.count(),len(db.docs.find_one()['topics'])),dtype=np.float32)
    print docs.shape
    i = 0
    for doc in db.docs.find():
        i += 1
        if i % 1000 == 0:
            print 'doc topic matrix', i
        docs[int(doc['_id']),:]=np.array(doc['topics'],dtype=np.float32)
    return np.array(docs)

def update_topic(db, topic_idx, terms, scores):
    '''
    topic is a list of terms
    scores is a list of scores for those terms
    '''
    db.topics.save({
        '_id': str(topic_idx),
        'terms': terms,
        'scores': scores
    })

def update_topic_docs(db, topic_idx, doc_ids):
    db.topics.update(
        {'_id': str(topic_idx)},
        {'$set': {'similar_docs': doc_ids}})


# mode is 0 for doc frequency, and 1 for word frequency
def get_rare_words(db,thresh=10,mode=0):
    rare_words={}
    field='doc_count'
    if mode:
        field='word_count'
    for term in db.terms.find({field:{'$lt':thresh}}):
        rare_words[term._id]=term.word
    return rare_words

# assume "common" means above a certain % of docs containing or total words
def get_common_words(db,thresh=0.2, mode=0):
    common_words={}
    field='doc_count'
    if mode:
        field='word_count'
    total=db.terms.find_one({'_id':'-1'})
    ntotal=total[field]
    for term in db.terms.find({field:{'$gt':int(thresh*ntotal)}}):
        common_words[term._id]=term.word
    if '-1' in common_words:
        del common_words['-1']
    return common_words

# get words that pass both lower and upper bound for the given mode
def get_valid_words(db, lower_thresh=10, upper_thresh=0.2, mode=0):
    total=db.terms.find_one({'_id':'-1'})
    field='doc_count'
    if mode:
        field='word_count'
    ntotal=total[field]
    upper_thresh=int(ntotal*upper_thresh)
    valid_words={}
    for term in db.terms.find({field:{'$lt':upper_thresh,'$gt':lower_thresh}}):
        valid_words[term._id]=term.word
    return valid_words

def get_id_from_word(db,word):
    item=db.terms.find_one({'word':word})
    return item._id

def get_word_from_id(db,idx):
    item=db.terms.find_one({'_id':str(idx)})
    return item.word

def get_doc_from_id(db,idx):
    item=db.docs.find_one({'_id':str(idx)})
    return item







