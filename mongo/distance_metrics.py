import numpy as np


def bhatt_distance(a, b):
    """
    Returns the Bhattacharyya distance between to discrete distributions.

    a is expected to be a 1d array (eg., a single document),
    b is expected to be a 2d array with observations over rows
    (eg., the rest of the documents in the corpus).
    """
    return -np.log(np.dot(b**.5, a**.5))

def euclidean_distance(a, b, axis=1):
    """
    Returns the Euclidean distance between two discrete distributions.

    a is expected to be a 1d array (eg., a single document),
    b is expected to be a 2d array with observations over rows
    (eg., the rest of the documents in the corpus).
    """
    return np.sum((a-b)**2, axis=axis)**.5
    #NOTE: the below be preferred for "big" comparisons in dim 1 of b
    #return np.apply_along_axis(np.linalg.norm, axis, doca-docb)

# careful, this isn't metric - doesn't satisfy triangle inequality
def kl_distance(a, b, a_log=None, axis=1, cached_log=1):
    """
    Measure of relative entropy between two discrete distributions.

    a is expected to be a 1d array (eg., a single document),
    b is expected to be a 2d array with observations over rows
    (eg., the rest of the documents in the corpus).
    """
    if cached_log:
        return np.sum(a*(a_log-b),axis=axis)
    return np.sum(a*(np.log(a)-np.log(b)), axis=axis)

def cosine_distance(a, b, axis=1):
    """
    Cosine distance between two vectors.

    Notes
    -----
    Closer to 1 means distance is smaller.
    """
    a_norm = np.dot(a,a)**.5
    b_norm = np.sum(b**2, axis=axis)**.5
    return np.abs(np.dot(b,a)/(a_norm*b_norm)-1)

def hellinger_distance(doca, docb, axis=1):
    """
    Returns the Hellinger Distance between documents.

    doca is expected to be a 1d array (ie., a single document),
    docb is expected to be a 2d array(ie., the rest of the documents in the
    corpus).

    Note that this expects to be given proper probability distributions.
    """
    return np.sum((doca**.5 - docb**.5)**2, axis=axis)

