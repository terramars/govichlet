'''
Writes topics and doc-topics
'''

import subprocess
import mongo_corpus
import numpy
import logging
import heapq
import os
from scipy.special import psi

FORMAT = '%(asctime)s %(funcName)s: %(message)s'
logging.basicConfig(format=FORMAT, level=logging.DEBUG)

MODEL_FILE = '/tmp/vw.model.dat.'
CACHE_FILE = '/tmp/vw.cache.'
PREDICTIONS_FILE = '/tmp/vw.predictions.'
READABLE_TOPICS_FILE = '/tmp/vw.topics.'

def file_len(fname):
    p = subprocess.Popen(['wc', '-l', fname], stdout=subprocess.PIPE, 
                                              stderr=subprocess.PIPE)
    result, err = p.communicate()
    if p.returncode != 0:
        raise IOError(err)
    return int(result.strip().split()[0])

def train_vw(num_topics, input_data_file):
    num_examples = file_len(input_data_file)
    alpha=5.0/num_topics
    rho=0.01
    vw_args = [
        'vw', '-d', input_data_file,
        '--lda', str(num_topics),
        '-b', '16',
        '-f', MODEL_FILE+str(num_topics),
        '--cache_file', CACHE_FILE+os.path.basename(input_data_file),
        '--lda_D', str(num_examples),
        '--lda_alpha',str(alpha),
        '--lda_rho',str(rho),
        '--initial_t','16',
        '--power_t','0.7',
        '--minibatch', '256',
        '--readable_model', READABLE_TOPICS_FILE+str(num_topics),
        '--passes','4',
    ]
    logging.debug('calling vowpal wabbit: %s' % ' '.join(vw_args))
    subprocess.call(vw_args)
    logging.debug('finished training with vowpal wabbit')

def predict_vw(num_topics, input_data_file):
    num_examples = file_len(input_data_file)
    alpha=5.0/num_topics
    rho=0.01
    vw_args = [
        'vw', '-d', input_data_file,
        '--lda', str(num_topics),
        '-b', '16', '-t',
        '--lda_alpha',str(alpha),
        '--lda_rho',str(rho),
        '-i', MODEL_FILE+str(num_topics),
        '-p', PREDICTIONS_FILE+os.path.basename(input_data_file)+'.'+str(num_topics),
    ]
    logging.debug('calling vowpal wabbit: %s' % ' '.join(vw_args))
    subprocess.call(vw_args)
    logging.debug('finished predicting with vowpal wabbit')

def normalize_list(l):
    a = numpy.array(l)
    return [i if i > .00001 else 0 for i in list(a / numpy.linalg.norm(a))]

def parse_predictions_file(db, predictions_file,normalize=True):
    logging.debug('parsing prediction file')
    with open(predictions_file, 'r') as source:
        for doc_idx, line in enumerate(source):
            #TODO: use something other than a list
            topics = [float(x) for x in line.split()]
            if normalize:
                topics = normalize_list(topics)
            mongo_corpus.update_doc_topics(db, doc_idx, topics)
            if doc_idx % 1000 == 0:
                logging.debug('finished parsing predictions: %d docs' % doc_idx)
    logging.debug('finished parsing predictions file')

def parse_topics_file(db, topics_file,start_lines=7):
    def dirichlet_expectation(alpha):
        return psi(alpha) - psi(numpy.sum(alpha))
    logging.debug('started parsing topics file')
    with open(topics_file, 'r') as source:
       #first 7 lines are garbage:
        for i in xrange(start_lines):
            logging.debug('throwing away line: %s' % source.readline())

        def topics_by_words_matrix():
            '''
            in theory this should work, but it runs out of memory when taking transpose?
            '''
            matrix_source = [numpy.array(map(float,l.split())) for l in source.readlines() if l.strip()]
            logging.debug('loaded matrix into string (%d, %d)' % (len(matrix_source), matrix_source[0].size))
            matrix_source = numpy.array(matrix_source).T
            logging.debug('loaded matrix into numpy matrix')
            logging.debug('took transpose')

            fout=open(topics_file+'.beta','wb')
            for i in xrange(matrix_source.shape[0]):
                fout.write(' '.join(map(str,dirichlet_expectation(matrix_source[i,:])))+'\n')
            fout.close()
            for topic_idx in xrange(matrix_source.shape[0]):
                #topic is a list of scores
                topic=matrix_source[topic_idx,:]
                tosort=topic.copy()
                #grab top 50 words
                filtered_words_for_topic = list(numpy.argsort(tosort)[::-1][:50])
                scores = [topic[i] for i in filtered_words_for_topic]
                mongo_corpus.update_topic(db, topic_idx, list(map(str,filtered_words_for_topic)), list(scores))
                logging.debug('finished topic index %d' % topic_idx)

        def topics_by_words_list():
            matrix_source = [[float(x) for x in l.split()] for l in source.readlines() if l.strip()]
            #for each topic, find the max words
            for topic_idx in xrange(len(matrix_source[0])):
                idx_scores = heapq.nlargest(10, enumerate([row[topic_idx] for row in matrix_source]), key = lambda x: x[1])
                topics = [i[0] for i in idx_scores]
                scores = [i[1] for i in idx_scores]
                mongo_corpus.update_topic(db, topic_idx, topics, scores)
                logging.debug('finished topic index %d' % topic_idx)

        topics_by_words_matrix()
                     
    logging.debug('finished parsing topics file')

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Generate relations and cache them in mongodb')
    parser.add_argument('--file', action="store", type=str)
    parser.add_argument('--db', action="store", default='bill-summaries', type=str)
    parser.add_argument('--host', action="store", default='localhost', type=str)
    parser.add_argument('--port', action="store", default='27017', type=str)
    parser.add_argument('--start_lines',action="store",default=7,type=int)
    parser.add_argument('--topics',action="store",default=50,type=int)
    parser.add_argument('--train',action="store_true",default=False)
    args = parser.parse_args()

    connection = mongo_corpus.connect(args.host, args.port)
    db = mongo_corpus.load_db(connection, args.db)
    
    if args.train:
        train_vw(args.topics, args.file)
        parse_topics_file(db, READABLE_TOPICS_FILE+str(args.topics),args.start_lines)
    predict_vw(args.topics, args.file)   
    parse_predictions_file(db, PREDICTIONS_FILE+os.path.basename(args.file)+'.'+str(args.topics))


