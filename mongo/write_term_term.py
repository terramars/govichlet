from mongo_corpus import *
import sys
from itertools import imap

import numpy as np
from numpy import argsort, asarray

from distance_metrics import kl_distance,euclidean_distance

def get_term_topic_matrix(source):
    matrix_source=np.array([np.array(map(float,l.split())) for l in source.readlines() if l.strip()])
    return np.exp(matrix_source)

def get_relations(a,b,a_log=None,cached_log=None):
    return kl_distance(a,b,a_log=a_log,cached_log=cached_log)

def get_top_terms(db,source,n=50):
    terms=get_term_topic_matrix(source)
    nterms=db.terms.count()
    terms=np.abs(terms.T/np.sum(terms.T,axis=0))[:nterms,:]
    print terms.shape
    log_terms=np.log(terms)
    log_terms=np.choose((log_terms<-1000),(log_terms,-1000))
    log_terms=np.choose((log_terms>1000),(log_terms,1000))
    #relations=np.zeros((docs.shape[0],docs.shape[0]),dtype=np.float32)
    for i in xrange(terms.shape[0]-1):
        #scores=get_doc_relations(docs[i,:],docs[i+1,:])
        scores=get_relations(terms[i,:],log_terms,a_log=log_terms[i,:],cached_log=True)
        #relations[i,i+1:]=scores
        #relations[i+1:,i]=scores
       # for i in xrange(docs.shape[0]):
        #to_compare=relations[i,:]
        to_compare=np.abs(scores)
        #if i%5000==0:
        #    print 'writing relations',i
        ids=argsort(to_compare)[0:n+1]    
        if i%1000==0:
            print 'found scores',i,ids,scores[ids]
        update_term_term(db,i,ids,to_compare[ids])
    return


if __name__=='__main__':
    name=sys.argv[1]
    fin=open(sys.argv[2])
    if len(sys.argv)>5:
        host=sys.argv[3]
        port=sys.argv[4]
        db=connect(host,port)
    else:
        db=connect()
    db=load_db(db,name)
    get_top_terms(db,fin) 

