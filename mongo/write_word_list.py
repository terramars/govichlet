from mongo_corpus import *
import sys

def load_word_list(fin):
    fin=open(fin)
    words=[line.strip() for line in fin]
    return words

def write_word_list(db,fin):
    words=load_word_list(fin)
    ids,i=insert_word_list(db,words)
    print 'inserted',i,'words'
    return

if __name__=='__main__':
    name=sys.argv[1]
    fin=sys.argv[2]
    if len(sys.argv)>5:
        host=sys.argv[3]
        port=sys.argv[4]
        db=connect(host,port)
    else:
        db=connect()
    db=load_db(db,name)
    initialize_index(db,'terms','word')
    write_word_list(db,fin)
