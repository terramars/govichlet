from mongo_corpus import *
from collections import Counter
import re
import sys

def load_id_file(fin):
    splitexp=re.compile(r'[ :]')
    wordids=[]
    wordcts=[]
    for line in open(fin):
        line=line.strip()
        splitline=[int(i) for i in splitexp.split(line[2:])]
        ids=splitline[0::2]
        cts=splitline[1::2]
        wordids.append(ids)
        wordcts.append(cts)
    return wordids,wordcts

def get_total_counts(wordids,wordcts):
    totalcts=Counter()
    docts=Counter()
    for i in xrange(len(wordids)):
        ids=wordids[i]
        cts=wordcts[i]
        for j in xrange(len(ids)):
            totalcts[ids[j]]+=cts[j]
            docts[ids[j]]+=1
    return totalcts,docts

def write_word_counts(db,wordids,wordcts):
    totals,docts=get_total_counts(wordids,wordcts)
    update_word_counts(db,totals.keys(),totals.values(),docts.values(),len(wordids))
    print 'wrote word counts'
    docs=[]
    for i in xrange(len(wordids)):
        docs.append(format_doc(i,wordids=wordids[i],wordcts=wordcts[i]))
    add_ids_and_cts_to_docs(db,docs)
    print 'wrote docs'

    

if __name__=='__main__':
    name=sys.argv[1]
    fin=sys.argv[2]
    if len(sys.argv)>5:
        host=sys.argv[3]
        port=sys.argv[4]
        db=connect(host,port)
    else:
        db=connect()
    db=load_db(db,name)
    wordids,wordcts=load_id_file(fin)
    write_word_counts(db,wordids,wordcts)
